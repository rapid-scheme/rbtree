;; Copyright (C) 2016 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid rbtree-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid receive)
	  (rapid rbtree))
  (begin
    (define (run-tests)

      (test-begin "Red-Black Trees")

      (test-assert "make-tree"
	(assume-tree < (make-tree)))

      (test-group "Insertion"
	(define (f x) (* x x))
	(define nums '(0 1 2 3 4 5 6 8 9))
	(define tree	    
	  (let loop ((elements '(1 3 5 7 9 2 4 6 8 0)) (tree (make-tree)))
	    (if (null? elements)
		tree
		(loop (cdr elements)
		      (update < tree (car elements) (f (car elements)))))))

	(test-assert (assume-tree < tree))

	(test-equal
	  (map f nums)
	  (map (lambda (key)
		 (ref < tree key))
	       nums))

	(test-assert (not (ref < tree 10))))

      (test-group "Deletion"
	(define (f x) (* x x))
	(define nums '(0 2 4 6 8))
	(define tree1 
	  (let loop ((elements '(1 3 5 7 9 2 4 6 8 0)) (tree (make-tree)))
	    (if (null? elements)
		tree
		(loop (cdr elements)
		      (update < tree (car elements) (f (car elements)))))))
	(define tree2
	  (let loop ((elements '(1 3 5 7 9)) (tree tree1))
	    (if (null? elements)
		tree
		(loop (cdr elements)
		      (delete < tree (car elements))))))
	
	(test-assert (assume-tree < tree2))

	(test-equal
	  (map f nums)
	  (map (lambda (key)
		 (ref < tree2 key))
	       nums))

	(test-assert (not (ref < tree2 1))))

      (test-group "Catenate"
	(define tree1 (new-tree 0 0 1 1 2 4))
	(define tree2 (new-tree 6 36 7 49 8 64 9 81 10 100 11 121))

	(test-assert (assume-tree < (tree-catenate tree1 5 25 tree2))))

      (test-group "Split"
	(define (f x) (* x x))
	(define nums '(0 1 2 3 4 5 6 8 9))
	(define tree	    
	  (let loop ((elements '(1 3 5 7 9 2 4 6 8 0)) (tree (make-tree)))
	    (if (null? elements)
		tree
		(loop (cdr elements)
		      (update < tree (car elements) (f (car elements)))))))
	(define-values (tree< tree<= tree= tree>= tree>)
	  (tree-split < tree 3))

       	(test-assert (assume-tree < tree<))
	(test-assert (assume-tree < tree<=))
	(test-assert (assume-tree < tree=))
	(test-assert (assume-tree < tree>=))
	(test-assert (assume-tree < tree>)))	  
      
      (test-end))

    (define (new-tree . items)
      (let loop ((items items)
		 (tree (make-tree)))
	(if (null? items)
	    tree
	    (loop (cddr items)
		  (update < tree (car items) (cadr items))))))
    
    (define (ref less? tree key)
      (receive (tree obj)
	  (tree-search less?
		       tree
		       key
		       (lambda (insert ignore)
			 (ignore #f))
		       (lambda (old-key old-value update remove)
			 (update old-key old-value old-value)))
	obj))
    
    (define (update less? tree key value)
      (receive (tree obj)
	  (tree-search less?
		       tree
		       key
		       (lambda (insert ignore)
			 (insert key value #f))
		       (lambda (old-key old-value update remove)
			 (update key value #f)))
	tree))

    (define (delete less? tree key)
      (receive (tree obj)
	  (tree-search less?
		       tree
		       key
		       (lambda (insert ignore)
			 (ignore #f))
		       (lambda (old-key old-value update remove)
			 (remove #f)))
	tree))))
